### Инсталляция Dynatrace OneAgent в OpenShift/Kubernetes кластер
- Скопируйте dynatrace-values-sample.yaml в dynatrace-values.yaml
- Добавьте в файл dynatrace-values.yaml ваш URL для скачивания агента и ваш PaaS токен Dynatrace
- [Как найти URL для скачивания и PaaS токен](https://www.dynatrace.com/support/help/setup-and-configuration/setup-on-container-platforms/kubernetes/deploy-daemonset/)
- Установите значения quota и resources для ваших неймспейса и экземпляра агента
- Запустите команду: `helm upgrade --install <ИМЯ РЕЛИЗА> ./helm/dynatrace-oneagent -f dynatrace-values.yaml -n <НЕЙМСПЕЙС> --create-namespace`
